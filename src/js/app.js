import "focus-visible"
import objectFitImages from "object-fit-images"
import { ratingInit } from "./components/rating"
import { commentInit } from "./components/comment"
import { headerInit } from "./components/header"

document.addEventListener("DOMContentLoaded", () => {
  objectFitImages()
  ratingInit()
  commentInit()
  headerInit()
})
