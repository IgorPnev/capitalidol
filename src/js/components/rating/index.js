import rater from "rater-js"
import { q } from "../../util/q"

export const ratingInit = () => {
  const ratingElement = q(".articleRatingJS")
  if (ratingElement) {
    const myRater = rater({
      element: ratingElement,
      showToolTip: false,
      rateCallback: function rateCallback(rating, done) {
        this.setRating(rating)
        done()
      }
    })
  }
}
