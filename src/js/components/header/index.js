import { q, qq } from "../../util/q"

export const headerInit = () => {
  const mobileButton = q(".headerMobileMenu")
  const mobileMenu = q(".mobileMenu")
  mobileButton.addEventListener("click", () => {
    mobileButton.classList.toggle("headerMobileMenuOpen")
    mobileMenu.classList.toggle("mobileMenuShow")
  })
}
