import { q } from "../../util/q"
import hyperform from "hyperform"

export const formInit = () => {
  const form = q(".articleCommentForm")
  if (form) {
    hyperform(window)
  }
}
