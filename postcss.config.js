/* eslint-disable no-unused-vars */

module.exports = ctx => ({
  plugins: {
    "postcss-import-ext-glob": {},
    "postcss-import": {},
    "postcss-nested": {},
    lost: {},
    "postcss-responsive-type": {},
    "postcss-center": {},
    "postcss-short": {},
    "postcss-easings": {},
    "postcss-flexbox": {},
    "postcss-flexbugs-fixes": {},
    "postcss-aspect-ratio": {},
    "postcss-clip-path-polyfill": {},
    "postcss-preset-env": {
      stage: 0
    },

    "postcss-css-reset": {},
    autoprefixer: {},
    // cssnano: {
    //   preset: "advanced"
    // }
    cssnano: ctx.env === "production" ? { preset: "advanced" } : false,
    "postcss-reporter": {}
  }
})
