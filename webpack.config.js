"use strict"

const webpack = require("webpack")
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin
const CleanWebpackPlugin = require("clean-webpack-plugin")
const UglifyJSPlugin = require("uglifyjs-webpack-plugin")
const path = require("path")

const cacheDir = path.resolve(__dirname, "..", "node_modules", ".cache")

const getThreadLoader = name => ({
  loader: "thread-loader",
  options: {
    workerParallelJobs: 50,
    poolRespawn: false,
    name
  }
})

module.exports = {
  devtool: "source-map",
  performance: { hints: false },
  watch: true,
  mode: "development",
  entry: {
    app: "./src/js/app.js"
  },
  output: {
    path: __dirname + "/build/js",
    chunkFilename: "[name].js",
    publicPath: "/js",
    filename: "[name].js",
    libraryTarget: "this",
    library: "[name]"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        // exclude: /node_modules\/(?!(dom7|ssr-window|swiper)\/).*/,

        use: [
          {
            loader: "cache-loader",
            options: {
              cacheDirectory: path.resolve(cacheDir, "js")
            }
          },
          getThreadLoader("js"),
          {
            loader: "babel-loader",
            options: {
              presets: ["@babel/env", "@babel/preset-react"],
              plugins: [
                "@babel/plugin-proposal-object-rest-spread",
                // "syntax-async-functions",
                "@babel/plugin-proposal-class-properties",
                // "transform-custom-element-classes",
                "@babel/plugin-transform-regenerator"

                // [
                //   "@babel/plugin-transform-runtime",
                //   {
                //     regenerator: true
                //   }
                // ]
              ]
            }
          }
        ]
      }
    ]
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          chunks: "initial",
          // minChunks: 2,
          name: "commons"
        }
      }
    },
    runtimeChunk: { name: "commons" }

    // splitChunks: {
    //   cacheGroups: {
    //     // default: false,
    //     // vendors: false,

    //     // vendor chunk
    //     vendor: {
    //       name: true,
    //       chunks: "all",
    //       test: /[\\/]node_modules[\\/]/,
    //       minChunks: 1,
    //       priority: 10
    //     },
    //     react: {
    //       name: true,
    //       chunks: "all",
    //       test: /react | rematch | flubber /,
    //       minChunks: 1,
    //       priority: 20,
    //       enforce: true
    //     }

    //     // common chunk
    //     // common: {
    //     //   name: "common",
    //     //   minChunks: 2,
    //     //   chunks: "all",
    //     //   priority: 30,
    //     //   reuseExistingChunk: true,
    //     //   enforce: true
    //     // }
    //   }
    // runtimeChunk: true
    // }
  },
  plugins: [
    // new webpack.ProvidePlugin({
    //   $: "jquery/dist/jquery.min.js",
    //   jQuery: "jquery/dist/jquery.min.js",
    //   "window.jQuery": "jquery/dist/jquery.min.js"
    // })
    // new BundleAnalyzerPlugin()
  ]
}

if (process.env.NODE_ENV === "production") {
  module.exports.devtool = false
  module.exports.watch = false
  module.exports.mode = "production"
  module.exports.plugins = (module.exports.plugins || []).concat([
    // new CleanWebpackPlugin(["**/*.js.map"], { root: __dirname + "./../local/templates/main", verbose: true }),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: '"production"'
      }
    }),
    new UglifyJSPlugin({
      sourceMap: false
    }),
    new BundleAnalyzerPlugin()
  ])
}
