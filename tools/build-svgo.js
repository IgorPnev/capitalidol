import { src, dest, watch, parallel, series, lastRun } from "gulp"
import plumber from "gulp-plumber"
import svgmin from "gulp-svgmin"

export const buildSvgo = () =>
  src("./src/svg/source/*.svg")
    .pipe(plumber())
    .pipe(svgmin({
        js2svg: {
            pretty: true
        },
        plugins: [{
            removeViewBox: false
        }]
    }))
    .pipe(dest("./src/svg/"))
