import { src, dest, watch, parallel, series, lastRun } from "gulp"
import revRewrite from "gulp-rev-rewrite"
import rename from "gulp-rename"
import replace from "gulp-replace-string"
export const replaceManifest = () => {
  const manifest = src("../local/templates/ilim/svg/rev-manifest.json")
  return src("./src/js/svg-loader-template.js")
    .pipe(replace("@src@", "/local/templates/ilim/svg/sprite.svg"))
    .pipe(revRewrite({ manifest }))
    .pipe(rename("svg-loader.js"))
    .pipe(dest("../local/templates/ilim/js/"))
    .pipe(replace("/local/templates/ilim/", "./"))
    .pipe(dest("./build/js/"))
}
