import { src, dest, watch, parallel, series, lastRun } from 'gulp'
import tinypng from 'gulp-tinypng-compress'

export const buildImage = () => src(['./src/img/**/*.{webp,png,jpg,jpeg}', '!./src/img/inspections/**/*.{webp,png,jpg,jpeg}']) // с svg не работает если включен tinypng
    // .pipe(tinypng({
    //     key: 'wFsD0dwWNjsvqdjfVOZI_hU8zq2sNvDD',
    //     sigFile: './src/img/.tinypng-sigs',
    //     log: true
    // }))
    .pipe(dest('./build/img'))
