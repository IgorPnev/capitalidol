import { dest, lastRun, parallel, series, src, watch } from "gulp"

import cache from "gulp-cached"
import concat from "gulp-concat"
import plumber from "gulp-plumber"
import postcss from "gulp-postcss"
import remember from "gulp-remember"
import rupture from "rupture"
import sourcemaps from "gulp-sourcemaps"
import util from "gulp-util"
import path from "path"
import cssimport from "gulp-cssimport"
import postcssPresetEnv from "postcss-preset-env"

const production = !!util.env.production

const dirs = {
  src: "src",
  dest: "build"
}

const sources = {
  // cssLayout: `${dirs.src}/css/layout/**/*.css`,
  // cssUi: `${dirs.src}/css/ui/**/*.css`,
  // css: `${dirs.src}/css/block/**/*.css`,
  // cssException: `!${dirs.src}/css/**/_*.css`
  css: `${dirs.src}/css/main.css`
}

export const buildCss = () =>
  src([sources.css])
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(postcss()) // Конфигурация берется из файла postcss.config.js
    .pipe(sourcemaps.write("./"))
    .pipe(dest(dirs.dest + "/css/"))

export const buildCssOld = (
  done,
  product = false // Этот таск не используется
) =>
  src([sources.css], {
    // sources.cssLayout, sources.cssUi, sources.css, sources.cssException
    // since: lastRun(buildCss)
  })
    .pipe(plumber())
    // .pipe(cache('css'))
    .pipe(sourcemaps.init())
    // .pipe(cssimport())
    .pipe(
      postcss([
        require("postcss-easy-import"),
        require("postcss-assets")({
          loadPaths: ["src/icons/", "src/images.css/", "src/svg"]
        }),
        require("lost"),
        require("postcss-flexbugs-fixes"),
        require("postcss-responsive-type"),
        postcssPresetEnv({
          stage: 0
        })
      ])
    )
    .on("error", done)
    .pipe(remember("css"))
    .pipe(concat("main.css"))
    .pipe(postcss([require("postcss-css-reset")]))
    .pipe(postcss([require("autoprefixer")]))
    .pipe(production ? postcss([require("gulp-csso")]) : util.noop())

    // .pipe(dest(dirs.dest + "/css/"))
    // .pipe(sourcemaps.write("."))
    .pipe(dest("../local/templates/ilim/css/"))
