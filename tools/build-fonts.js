import { src, dest, watch, parallel, series, lastRun } from 'gulp'

export const buildFonts = () => src('src/fonts/**/*.{woff,woff2}')
    .pipe(dest('build/fonts/'))
